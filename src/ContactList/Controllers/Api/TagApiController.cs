﻿using System;
using Microsoft.AspNetCore.Mvc;
using ContactList.Models;
using System.Net;

namespace ContactList.Controllers.Api
{
    [Route("api/tags")]
    public class TagApiController : Controller
    {
        private IContactListRepository _repository;

        public TagApiController(IContactListRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("")]
        public JsonResult Get()
        {
            try
            {
                var results = _repository.GetAllTags();
                return Json(results);
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = ex.Message });
            }
            
        }

        [HttpPost("")]
        public JsonResult Post([FromBody]ContactTag tag)
        {
            try
            {
                _repository.AddTag(tag);
                if (_repository.SaveAll())
                {
                    Response.StatusCode = (int)HttpStatusCode.Created;
                    return Json(tag);
                }
                
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed", ModelState = ModelState });
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            var results = _repository.GetAllTags();
            foreach (ContactTag tag in results)
            {
                if (tag.ContactTagId == id)
                {
                    _repository.DeleteTag(tag);
                }

            }
            if (_repository.SaveAll())
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(true);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed" });
        }


    }
}
