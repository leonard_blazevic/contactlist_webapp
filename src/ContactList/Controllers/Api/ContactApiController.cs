﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ContactList.Models;
using System.Net;
using ContactList.ViewModels;
using AutoMapper;

namespace ContactList.Controllers.Api
{
    //Apii za dohvacanje (svih konatkata, prema ideu, tagu, imenu), update, brisanje kontakata kojima se pristupa na client side-u
    //komunikacija s bazom se odvija preko pomocne klase ContactListRepository
    [Route("api/contacts")]
    public class ContactApiController : Controller
    {
        private IContactListRepository _repository;

        public ContactApiController (IContactListRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("")]
         public JsonResult Get ()
         {
             var results = Mapper.Map<IEnumerable<ContactViewModel>>(_repository.GetAllContactsFull());
             return Json(results);
         }

        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        { 
            try
            {
                ContactViewModel contact = Mapper.Map<IEnumerable<ContactViewModel>>(_repository.GetContactsById(id)).First();
                return Json(contact);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = "Failed" + ex.Message });
            }


        }

        [HttpGet("byTag/{tag}")]
        public JsonResult GetByTag(int tag)
        {
            List<Contact> results = new List<Contact>();
            var contacts = _repository.GetAllContactsFull();

            //Zbog greške s id-evima (nisu bili slijedni) kod kreiranja baze podataka postojali su problemi kad bi se filtracija vršila na bazi 
            //pa je ovo bila "zakrpa", ali inače bi umjesto ove for petlje napravio metodu u repositoryiu koja iz 
            //baze vrać samo odgovarajuće kontakte

            foreach (Contact contact in contacts)
            {
                foreach(Tag tg in contact.TagValues)
                {
                    if (tg.TagValue == tag)
                        results.Add(contact);
                }
            }
            try
            {
                return Json(results.OrderBy(t => t.Name));
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = "Failed" + ex.Message });
            }
        }

        [HttpGet("byString/{txt}")]
        public JsonResult GetByString(String txt)
        {
            var results = Mapper.Map<IEnumerable<ContactViewModel>>(_repository.GetContactsByString(txt));
            try
            {
                return Json(results);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = "Failed" + ex.Message });
            }
        }


        [HttpPost("")]
        public JsonResult Post([FromBody]ContactViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newContact = Mapper.Map<Contact>(vm);
                    _repository.AddContact(newContact);
                    if (_repository.SaveAll())
                    {
                        Response.StatusCode = (int)HttpStatusCode.Created;
                        return Json(Mapper.Map<ContactViewModel>(newContact));
                    }
                }
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = ex.Message });
            }
            
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed", ModelState = ModelState});
        }

        [HttpDelete("delTags/{id}")]
        public JsonResult DelTags(int id)
        {
            var results = _repository.GetTagValues();
            foreach (Tag tag in results)
            {
                if (tag.TagId == id)
                {
                    _repository.DeleteTagValue(tag);
                }

            }
            if (_repository.SaveAll())
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(true);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed" });

        }

        [HttpPut("{id}")]
        public JsonResult Put(int id, [FromBody]ContactViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newContact = Mapper.Map<Contact>(vm);
                    _repository.PutContact(newContact);
                    if (_repository.SaveAll())
                    {
                        Response.StatusCode = (int)HttpStatusCode.Created;
                        return Json(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed", ModelState = ModelState });
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            var results = _repository.GetAllContactsFull();
            
            foreach (Contact contact in results)
            {
                if (contact.ContactId == id)
                {
                    _repository.DeleteContact(contact);
                    foreach (ContactEmail email in contact.Emails)
                    {
                        _repository.DeleteEmail(email);
                    }
                    foreach (ContactPhoneNumber number in contact.PhoneNumbers)
                    {
                        _repository.DeleteNumber(number);
                    }
                }
                    
            }
            if (_repository.SaveAll())
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(true);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed" });

            
        }

    }
}
