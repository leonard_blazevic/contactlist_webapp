﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContactList.Models;
using System.Net;
using ContactList.ViewModels;
using AutoMapper;


namespace ContactList.Controllers.Api
{
    [Route("api/emails")]
    public class EmailApiController : Controller
    {
        private IContactListRepository _repository;

        public EmailApiController(IContactListRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("")]
        public JsonResult Get()
        {
            try
            {
                var results = _repository.GetAllEmails();
                return Json(results);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { Message = ex.Message });
            }

        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            var results = _repository.GetAllEmails();
            foreach (ContactEmail email in results)
            {
                if (email.ContactEmailId == id)
                {
                    _repository.DeleteEmail(email);
                }

            }
            if (_repository.SaveAll())
            {
                Response.StatusCode = (int)HttpStatusCode.Created;
                return Json(true);
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed" });
        }


    }
}
