﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ContactList.Models;

namespace ContactList.Migrations
{
    [DbContext(typeof(ContactsContext))]
    [Migration("20161113113850_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ContactList.Models.Contact", b =>
                {
                    b.Property<int>("ContactId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Birthday");

                    b.Property<string>("Name");

                    b.HasKey("ContactId");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("ContactList.Models.ContactEmail", b =>
                {
                    b.Property<int>("ContactEmailId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactId");

                    b.Property<string>("Email");

                    b.HasKey("ContactEmailId");

                    b.HasIndex("ContactId");

                    b.ToTable("ContactEmail");
                });

            modelBuilder.Entity("ContactList.Models.ContactPhoneNumber", b =>
                {
                    b.Property<int>("ContactPhoneNumberId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactId");

                    b.Property<int>("Number");

                    b.HasKey("ContactPhoneNumberId");

                    b.HasIndex("ContactId");

                    b.ToTable("ContactPhoneNumber");
                });

            modelBuilder.Entity("ContactList.Models.ContactEmail", b =>
                {
                    b.HasOne("ContactList.Models.Contact")
                        .WithMany("Emails")
                        .HasForeignKey("ContactId");
                });

            modelBuilder.Entity("ContactList.Models.ContactPhoneNumber", b =>
                {
                    b.HasOne("ContactList.Models.Contact")
                        .WithMany("PhoneNumbers")
                        .HasForeignKey("ContactId");
                });
        }
    }
}
