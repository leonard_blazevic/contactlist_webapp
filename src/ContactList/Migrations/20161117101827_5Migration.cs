﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContactList.Migrations
{
    public partial class _5Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tag",
                table: "Contacts");

            migrationBuilder.CreateTable(
                name: "ContactTags",
                columns: table => new
                {
                    ContactTagId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tag = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactTags", x => x.ContactTagId);
                });

            migrationBuilder.AddColumn<int>(
                name: "TagContactTagId",
                table: "Contacts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_TagContactTagId",
                table: "Contacts",
                column: "TagContactTagId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_ContactTags_TagContactTagId",
                table: "Contacts",
                column: "TagContactTagId",
                principalTable: "ContactTags",
                principalColumn: "ContactTagId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_ContactTags_TagContactTagId",
                table: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_Contacts_TagContactTagId",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "TagContactTagId",
                table: "Contacts");

            migrationBuilder.DropTable(
                name: "ContactTags");

            migrationBuilder.AddColumn<int>(
                name: "Tag",
                table: "Contacts",
                nullable: false,
                defaultValue: 0);
        }
    }
}
