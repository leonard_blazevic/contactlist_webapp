﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactList.Migrations
{
    public partial class _3Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContactEmail_Contacts_ContactId",
                table: "ContactEmail");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactPhoneNumber_Contacts_ContactId",
                table: "ContactPhoneNumber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactPhoneNumber",
                table: "ContactPhoneNumber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactEmail",
                table: "ContactEmail");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactPhoneNumbers",
                table: "ContactPhoneNumber",
                column: "ContactPhoneNumberId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactEmails",
                table: "ContactEmail",
                column: "ContactEmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactEmails_Contacts_ContactId",
                table: "ContactEmail",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "ContactId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContactPhoneNumbers_Contacts_ContactId",
                table: "ContactPhoneNumber",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "ContactId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.RenameIndex(
                name: "IX_ContactPhoneNumber_ContactId",
                table: "ContactPhoneNumber",
                newName: "IX_ContactPhoneNumbers_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_ContactEmail_ContactId",
                table: "ContactEmail",
                newName: "IX_ContactEmails_ContactId");

            migrationBuilder.RenameTable(
                name: "ContactPhoneNumber",
                newName: "ContactPhoneNumbers");

            migrationBuilder.RenameTable(
                name: "ContactEmail",
                newName: "ContactEmails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContactEmails_Contacts_ContactId",
                table: "ContactEmails");

            migrationBuilder.DropForeignKey(
                name: "FK_ContactPhoneNumbers_Contacts_ContactId",
                table: "ContactPhoneNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactPhoneNumbers",
                table: "ContactPhoneNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactEmails",
                table: "ContactEmails");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactPhoneNumber",
                table: "ContactPhoneNumbers",
                column: "ContactPhoneNumberId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactEmail",
                table: "ContactEmails",
                column: "ContactEmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactEmail_Contacts_ContactId",
                table: "ContactEmails",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "ContactId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContactPhoneNumber_Contacts_ContactId",
                table: "ContactPhoneNumbers",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "ContactId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.RenameIndex(
                name: "IX_ContactPhoneNumbers_ContactId",
                table: "ContactPhoneNumbers",
                newName: "IX_ContactPhoneNumber_ContactId");

            migrationBuilder.RenameIndex(
                name: "IX_ContactEmails_ContactId",
                table: "ContactEmails",
                newName: "IX_ContactEmail_ContactId");

            migrationBuilder.RenameTable(
                name: "ContactPhoneNumbers",
                newName: "ContactPhoneNumber");

            migrationBuilder.RenameTable(
                name: "ContactEmails",
                newName: "ContactEmail");
        }
    }
}
