﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ContactList.Models;

namespace ContactList.Migrations
{
    [DbContext(typeof(ContactsContext))]
    partial class ContactsContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ContactList.Models.Contact", b =>
                {
                    b.Property<int>("ContactId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Adress");

                    b.Property<string>("Birthday");

                    b.Property<string>("Name");

                    b.HasKey("ContactId");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("ContactList.Models.ContactEmail", b =>
                {
                    b.Property<int>("ContactEmailId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactId");

                    b.Property<string>("Email");

                    b.HasKey("ContactEmailId");

                    b.HasIndex("ContactId");

                    b.ToTable("ContactEmails");
                });

            modelBuilder.Entity("ContactList.Models.ContactPhoneNumber", b =>
                {
                    b.Property<int>("ContactPhoneNumberId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactId");

                    b.Property<string>("Number");

                    b.HasKey("ContactPhoneNumberId");

                    b.HasIndex("ContactId");

                    b.ToTable("ContactPhoneNumbers");
                });

            modelBuilder.Entity("ContactList.Models.ContactTag", b =>
                {
                    b.Property<int>("ContactTagId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Tag");

                    b.HasKey("ContactTagId");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("ContactList.Models.Tag", b =>
                {
                    b.Property<int>("TagId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContactId");

                    b.Property<int>("TagValue");

                    b.HasKey("TagId");

                    b.HasIndex("ContactId");

                    b.ToTable("TagValues");
                });

            modelBuilder.Entity("ContactList.Models.ContactEmail", b =>
                {
                    b.HasOne("ContactList.Models.Contact")
                        .WithMany("Emails")
                        .HasForeignKey("ContactId");
                });

            modelBuilder.Entity("ContactList.Models.ContactPhoneNumber", b =>
                {
                    b.HasOne("ContactList.Models.Contact")
                        .WithMany("PhoneNumbers")
                        .HasForeignKey("ContactId");
                });

            modelBuilder.Entity("ContactList.Models.Tag", b =>
                {
                    b.HasOne("ContactList.Models.Contact")
                        .WithMany("TagValues")
                        .HasForeignKey("ContactId");
                });
        }
    }
}
