﻿using Microsoft.EntityFrameworkCore;


namespace ContactList.Models
{
    public class ContactsContext : DbContext
    {
        public ContactsContext(DbContextOptions<ContactsContext> options)
            : base(options)
        { }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactPhoneNumber> ContactPhoneNumbers { get; set; }
        public DbSet<ContactEmail> ContactEmails { get; set; }
        public DbSet<ContactTag> Tags { get; set; }
        public DbSet<Tag> TagValues { get; set; }

    }
}
