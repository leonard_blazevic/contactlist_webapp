﻿using System;

namespace ContactList.Models
{
    //moguća promjena String => string
    public class ContactPhoneNumber
    {
        public int ContactPhoneNumberId { get; set; }
        public String Number { get; set; }
    }
}
