﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactList.Models
{
    public class Tag
    {
        public int TagId { get; set; }
        public int TagValue { get; set; }
    }
}
