﻿using System.Collections.Generic;
using System.Linq;



namespace ContactList.Models
{
    public class ContactsSampleDataSeeder
    {
        //Pomocna klasa koja pri prvom pokretanju puni bazu testnim podacima
        private ContactsContext _context;
        public ContactsSampleDataSeeder (ContactsContext context)
        {
            _context = context;
        }

        public void SeedData ()
        {
            if(!_context.Contacts.Any())
            {
                ContactTag tagFamily = new ContactTag()
                {
                    Tag="Family"
                };
                ContactTag tagFriends = new ContactTag()
                {
                    Tag="Friends"
                };
                ContactTag tagBusiness = new ContactTag()
                {
                    Tag="Business"
                };
                ContactTag tagOther = new ContactTag()
                {
                    Tag = "Other"
                };


                Contact contactLeo = new Contact()
                {
                    Name = "Leo",
                    Birthday="02.09.1996.",
                    Adress = "Put pecina 19",
                    TagValues = new List<Tag>()
                    {
                        new Tag()
                        {
                            TagValue=0
                        },
                        new Tag()
                        {
                            TagValue=1
                        }
                    },
                    PhoneNumbers = new List<ContactPhoneNumber>()
                    {
                        new ContactPhoneNumber()
                        {
                            Number="0996897977"
                        },
                        new ContactPhoneNumber()
                        {
                            Number="021880709"
                        }
                    },
                    Emails = new List<ContactEmail>()
                    {
                        new ContactEmail()
                        {
                            Email="leo1.blazevic@gmail.com"
                        },
                        new ContactEmail()
                        {
                            Email="leonard.blazevic@fer.hr"
                        }
                    }               
                };
                Contact contactAndela = new Contact()
                {
                    Name = "Andela",
                    Birthday = "09.01.1998.",
                    Adress = "Sv. Liberana 20",
                    TagValues = new List<Tag>()
                    {
                        new Tag()
                        {
                            TagValue=2
                        },
                        new Tag()
                        {
                            TagValue=3
                        }
                    },
                    PhoneNumbers = new List<ContactPhoneNumber>()
                    {
                        new ContactPhoneNumber()
                        {
                            Number="0955500138"
                        }
                    },
                    Emails = new List<ContactEmail>()
                    {
                        new ContactEmail()
                        {
                            Email="ababaja@gmail.com"
                        }
                    }
                };

                _context.Contacts.Add(contactLeo);
                _context.ContactPhoneNumbers.AddRange(contactLeo.PhoneNumbers);
                _context.ContactEmails.AddRange(contactLeo.Emails);

                _context.Contacts.Add(contactAndela);
                _context.ContactPhoneNumbers.AddRange(contactAndela.PhoneNumbers);
                _context.ContactEmails.AddRange(contactAndela.Emails);

                _context.Tags.Add(tagFamily);
                _context.Tags.Add(tagFriends);
                _context.Tags.Add(tagBusiness);
                _context.Tags.Add(tagOther);

                _context.SaveChanges();

            }
        }
    }
}
