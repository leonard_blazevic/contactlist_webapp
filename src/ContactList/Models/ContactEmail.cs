﻿using System;

namespace ContactList.Models
{
    //moguća promjena String => string
    public class ContactEmail
    {
        public int ContactEmailId { get; set; }
        public String Email { get; set; }
    }
}
