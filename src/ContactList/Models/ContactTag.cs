﻿using System;

namespace ContactList.Models
{
    //moguća promjena String => string
    public class ContactTag
    {
        public int ContactTagId { get; set; }
        public String Tag { get; set; }
    }
}
