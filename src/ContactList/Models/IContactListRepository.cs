﻿using System.Collections.Generic;

namespace ContactList.Models
{
    public interface IContactListRepository
    {
        IEnumerable<Contact> GetAllContactsFull();
        bool SaveAll();
        void AddContact(Contact newContact);
        void DeleteContact(Contact contact);
        void PutContact(Contact newContact);
        IEnumerable<ContactTag> GetAllTags();
        void AddTag(ContactTag tag);
        void DeleteTag(ContactTag tag);
        IEnumerable<Contact> GetContactsByTag(int tag);
        IEnumerable<Contact> GetContactsByString(string txt);
        IEnumerable<Contact> GetContactsById(int id);
        IEnumerable<ContactPhoneNumber> GetAllNumbers();
        void DeleteNumber(ContactPhoneNumber number);
        IEnumerable<ContactEmail> GetAllEmails();
        void DeleteEmail(ContactEmail email);
        void DeleteTagValue(Tag tag);
        IEnumerable<Tag> GetTagValues();
    }
}