﻿using System;
using System.Collections.Generic;

namespace ContactList.Models
{
    //moguća promjena String => string
    public class Contact
    {
        public int ContactId { get; set; }
        public String Name { get; set; }
        public List<ContactPhoneNumber> PhoneNumbers { get; set; }
        public List<ContactEmail> Emails { get; set; }
        public String Adress { get; set; }
        public String Birthday { get; set; }
        public List<Tag> TagValues { get; set; }

    }

    public enum Category
    {
        Family,
        Friends, 
        Business, 
        Other
    }
}
