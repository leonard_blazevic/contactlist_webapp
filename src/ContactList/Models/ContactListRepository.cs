﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ContactList.Models
{
    public class ContactListRepository : IContactListRepository
    {
        //pomocna klasa za komunikaciju s bazom podataka

        //moguće bi bilo razdvajanje ovog repositorya na više tematski bliskih repositorya zbog preglednosti ()

        private ContactsContext _context;
        public ContactListRepository (ContactsContext context)
        {
            _context = context;
        }

        public IEnumerable<Contact> GetAllContactsFull()
        {
            return _context.Contacts
                .Include(t => t.PhoneNumbers)
                .Include(t => t.Emails)
                .Include(t => t.TagValues)
                .OrderBy(t => t.Name)
                .ToList();
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }

        public void AddContact(Contact newContact)
        {
            _context.Add(newContact);
        }

        public void DeleteContact(Contact contact)
        {
            _context.Remove(contact);
        }

        public void PutContact(Contact newContact)
        {
            _context.Update(newContact);
        }

        public IEnumerable<ContactTag> GetAllTags()
        {
            return _context.Tags.ToList();
        }

        public void AddTag(ContactTag tag)
        {
            _context.Tags.Add(tag);
        }

        public void DeleteTag(ContactTag tag)
        {
            _context.Tags.Remove(tag);
        }

        public IEnumerable<Contact> GetContactsByTag(int tag)
        {
            return _context.Contacts
                //.Where(e => e.TagValues.Contains(f => f.TagValue==tag))
                .Include(t => t.PhoneNumbers)
                .Include(t => t.Emails)
                .Include(t => t.TagValues)
                .OrderBy(t => t.Name)
                .ToList();
        }

        public IEnumerable<Contact> GetContactsByString(string txt)
        {
            return _context.Contacts
                .Where(e => e.Name.Contains(txt))
                .Include(t => t.PhoneNumbers)
                .Include(t => t.Emails)
                .Include(t => t.TagValues)
                .OrderBy(t => t.Name)
                .ToList();
        }

        public IEnumerable<Contact> GetContactsById(int id)
        {
            return _context.Contacts
                .Where(e => e.ContactId==id)
                .Include(t => t.PhoneNumbers)
                .Include(t => t.Emails)
                .Include(t => t.TagValues)
                .ToList();
        }

        public IEnumerable<ContactPhoneNumber> GetAllNumbers()
        {
            return _context.ContactPhoneNumbers.ToList();
        }

        public void DeleteNumber(ContactPhoneNumber number)
        {
            _context.ContactPhoneNumbers.Remove(number);
        }

        public IEnumerable<ContactEmail> GetAllEmails()
        {
            return _context.ContactEmails.ToList();
        }

        public void DeleteEmail(ContactEmail email)
        {
            _context.ContactEmails.Remove(email);
        }

        public void DeleteTagValue(Tag tag)
        {
            _context.TagValues.Remove(tag);
        }

        public IEnumerable<Tag> GetTagValues()
        {
            return _context.TagValues.ToList();
        }
    }
}
