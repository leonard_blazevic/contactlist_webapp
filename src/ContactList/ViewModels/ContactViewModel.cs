﻿using ContactList.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContactList.ViewModels
{
    public class ContactViewModel
    {
        public int ContactId { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 3)]
        public String Name { get; set; }
        public List<ContactPhoneNumber> PhoneNumbers { get; set; }
        public List<ContactEmail> Emails { get; set; }
        public String Adress { get; set; }
        public String Birthday { get; set; }
        public List<Tag> TagValues { get; set; }
    }
}
