﻿(function () {
    "use strict";
    //Controller zaduzen za stvaranje novog kontakta sa proizvoljnim brojem emailova i telefonskoh brojeva,
    //za stvaranje nove kategorije koja po stvaranju automatski postaje dostupna u drop-down izborniku,
    //kontakt je moguće unijeti tek kada su zadovoljeni svi uvjeti polja za unos
    angular.module("contactsModule").controller("createNewController", createNewController);
    function createNewController($http, $location, $route) {
        var vm = this;
        vm.addedNumbers = 0;
        vm.addedEmails = 0;
        vm.phoneNumbers = [
            {value:"", id:0, name:"0"}
        ]
        vm.emails = [
            { value: "", id: 0, name: "0" }
        ]

        vm.newContact = {};
        vm.newContactFinished = {
            name: null,
            adress: null,
            birthday: null,
            emails: [],
            phoneNumbers: []
        };

        vm.currentTags = [];
        vm.finishedTags = [];
        vm.tags = [];
        vm.newTag = "";
        vm.currentTag = 1;
        vm.indexTag=1;

        $http.get("/api/tags").then(function (response) {
            vm.tags = response.data;
        }, function () {
        });

        vm.addTag = function () {
            var addTag = {
                tag:""
            };
            addTag.tag = vm.newTag;
            $http.post("/api/tags", addTag).then(function () {
                $http.get("/api/tags").then(function (response) {
                    vm.tags = response.data;
                    alert("Adding new category was successful!!!");
                }, function () {
                });
            }, function () {
                alert("Failed!!!!!");
            });
        };

        //kompliciranije izvedeno zbog greške s id-evima
        vm.setTag = function () {
            vm.finishedTags = [];
            vm.currentTags.forEach(function (curr) {
                vm.tags.forEach(function (tg, index) {
                    if(curr == tg.contactTagId)
                    {
                        var tag = { tagValue: index };
                        vm.finishedTags.push(tag);
                    }
                });
            });
        };

        vm.addNumber = function () {
            vm.addedNumbers++;
            var numName = vm.addedNumbers.toString();
            var number = { value: "", id: vm.addedNumbers, name: numName };
            vm.phoneNumbers.push(number);
        };

        vm.removeNumber = function () {
            vm.addedNumbers--;
            vm.phoneNumbers.pop();
        };

        vm.addEmail = function () {
            vm.addedEmails++;
            var emName = vm.addedEmails.toString();
            var email = { value: "", id: vm.addedEmails, name: emName };
            vm.emails.push(email);
        };

        vm.removeEmail = function () {
            vm.addedEmails--;
            vm.emails.pop();
        };

        vm.clearTags = function () {
            vm.currentTags = [];
            vm.setTag();
        };

        vm.addContact = function () {
            vm.newContactFinished.name = vm.newContact.name;
            vm.newContactFinished.adress = vm.newContact.adress;
            vm.newContactFinished.birthday = vm.newContact.birthday;
            vm.newContactFinished.tagValues = vm.finishedTags;

            vm.phoneNumbers.forEach(function (curr) {
                var num = { number: "" };
                num.number = curr.value;
                vm.newContactFinished.phoneNumbers[curr.id] = num;
            });
            vm.emails.forEach(function (curr) {
                var em = { email: "" };
                em.email = curr.value;
                vm.newContactFinished.emails[curr.id] = em;
            });

            $http.post("/api/contacts", vm.newContactFinished).then(function () {
                $location.path("/");
            }, function () {
                alert("Failed!!!!!");
            });
        };
    }
})();