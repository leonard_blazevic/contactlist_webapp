﻿(function () {
    //Controller zaduzen za pocetni ekran prikazivanje svih konatkata, 
    //slanje zahtjeva za filtriranje i trazenje prikazivanje dohvacenih podataka, 
    //sortiranje prema imenu koje se obavlja client-side za razliku od trazenja i filtriranja,
    //dohvacanje pojedinacnog kontakta klikom na njega te "linkove" prem aostalim djelovima stranice

    //1) pri pokretanju aplikacije se dohvaćaju svi kontakti zbog uvjeta zadatka, 
    //no u slučaju povećavanja broja kontakata uveo bi stranice i početno dohvaćanje samo prvih nekoliko kontakata zbog brzine (npr. prvih 30)
    //2) dio koda bi prebacio u service/repository zbog preglednosti i efikasnosti
    "use strict";
    angular.module("contactsModule").controller("contactsController", contactsController);
    function contactsController($http, $location)
    {
        var vm = this;
        vm.searchText = "";
        vm.contacts = [];
        vm.currentContact = {};
        vm.tags = [];
        vm.currentFilter = 0;
        vm.indexTag = 0;
        vm.sorts = [{ name: "By name ascending", id: 0 },
                    { name: "By name descending", id: 1 }];
        vm.currentSort = vm.sorts[0].id;

        //dohvaćanje tagova
        $http.get("/api/tags").then(function (response) {
            vm.tags = response.data;
        }, function () {
        });

        //dohvaćanje kontakata
        $http.get("/api/contacts").then(function (response) {
            vm.contacts = response.data;
        }, function () {
        });

        vm.search = function () {
            $http.get("/api/contacts/byString/" + vm.searchText).then(function (response) {
                vm.contacts = response.data;
            }, function () {
            });
        };

        //može se riješiti bez bespotrebnog upita na bazu pošto su na početku već dohvaćeni svi kontakti (npr. uvođenjem liste contactsForView) 
        vm.showAll = function () {
            $http.get("/api/contacts").then(function (response) {
                vm.contacts = response.data;
            }, function () {
            });
        };

        //komplicirano sortiranje i filtriranje po tagovima zbog greške kod id-eva u bazi (zakrpa zbog manjka vremena i znanja kod prve izrade)
        vm.filter = function () {
            
            vm.tags.forEach(function (curr, index) {
                if (curr.contactTagId == vm.currentFilter) {
                    vm.indexTag = index;
                }
            });
            $http.get("/api/contacts/byTag/" + vm.indexTag).then(function (response) {
                vm.contacts = response.data;
            }, function () {
            });
        };

        vm.sort = function () {
            if (vm.currentSort===1)
                vm.contacts.sort(function compare(a, b) {
                    return -1*(a.name.localeCompare(b.name));
                });
            else
                vm.contacts.sort(function compare(a, b) {
                    return a.name.localeCompare(b.name);
                });
            alert(sortBy);
        };

        vm.contactClick = function (contact) {
            $http.get("/api/contacts/" + contact.contactId).then(function (response) {
                vm.currentContact = response.data;
                $location.path("/Contact/" + vm.currentContact.name + "/" + vm.currentContact.contactId);
            }, function () {
                alert("error");
            });

        };
        
    }
})();