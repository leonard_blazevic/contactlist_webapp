﻿(function () {
    "use strict";
    angular.module("contactsModule", ["ngRoute"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                controller: "contactsController",
                controllerAs: "vm",
                templateUrl: "/views/contactsView.html",
            })
            .when("/CreateNew", {
                controller: "createNewController",
                controllerAs: "vm",
                templateUrl: "/views/createNewView.html"
            })
            .when("/Contact/:contactName/:contactId", {
                controller: "viewContactController",
                controllerAs: "vm",
                templateUrl: "/views/viewContactView.html"
            })
            .when("/EditContact/:contactName/:contactId", {
                controller: "editContactController",
                controllerAs: "vm",
                templateUrl: "/views/editContactView.html"
            })
            .otherwise({ redirectTo: "/" });
    });
})();