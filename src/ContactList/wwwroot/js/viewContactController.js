﻿(function () {
    "use strict";
    //controller odgovoran za prikaz pojedinacnog kontakta u cijelosti
    //sadrzi link prema uredjivanju kontakta i mogucnost brisanja istog
    angular.module("contactsModule").controller("viewContactController", viewContactController);
    function viewContactController($http, $location,$routeParams) {
        var vm = this;
        vm.currentContact = {};
        vm.tags = [];

        $http.get("/api/tags").then(function (response) {
            vm.tags = response.data;
        }, function () {
        });
        
        $http.get("/api/contacts/" + $routeParams.contactId).then(function (response) {
            vm.currentContact = response.data;
        }, function () {
            alert("error");
        });

        vm.deleteContact = function () {
            $http.delete("/api/contacts/" + $routeParams.contactId).then(function (response) {
                $location.path("/");
            }, function () {
                alert("error");
            });
        };

        vm.editContact = function () {
            $location.path("/EditContact/" + vm.currentContact.name + "/" + vm.currentContact.contactId);
        };
    }
})();