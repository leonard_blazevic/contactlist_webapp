﻿(function () {
    //Controller koji dohvaca podatke za zeljeni kontakt i prikazuje ih u poljima za input koja omogucavaju mijenjanje podataka
    //klikom na submit kontakt se sprema u bazu  aplikacija se vraca na pocetni ekran gdej je uredjeni kontakt vidljiv u novom obliku
    "use strict";
    angular.module("contactsModule").controller("editContactController", editContactController);
    function editContactController($http, $location, $routeParams) {
        var vm = this;
        vm.editedContact = {};
        vm.tags = [];
        vm.newTag = "";
        vm.currentTag = 0;
        vm.currentTags = [];
        vm.finishedTags = [];
        vm.initialTags = [];
        vm.initTags = [];
        vm.indexTag = 1;
        vm.addedNumbers = 0;
        vm.addedEmails = 0;
        vm.initialNumbers = 0;
        vm.initialEmails = 0;
        vm.phoneNumbers = [];
        vm.emails = [];
        vm.editedContactFinished = {
            name: null,
            adress: null,
            birthday: null,
            emails: [],
            phoneNumbers: []

        };

        //kompliciranije izvedeno zbog greške s id-evima
        vm.addTag = function () {
            var addTag = {
                tag: ""
            };
            addTag.tag = vm.newTag;
            $http.post("/api/tags", addTag).then(function () {
                $http.get("/api/tags").then(function (response) {
                    vm.tags = response.data;
                    alert("Adding new category was successful!!!");
                }, function () {
                });
            }, function () {
                alert("Failed!!!!!");
            });
        };

        vm.addNumber = function () {
            vm.addedNumbers++;
            var numName = vm.addedNumbers.toString();
            var number = { value: "", id: vm.addedNumbers, name: numName, realId: null };
            vm.phoneNumbers.push(number);
        };

        vm.removeNumber = function () {
            vm.addedNumbers--;
            vm.phoneNumbers.pop();
            if (!(vm.phoneNumbers.length > 0))
                vm.addNumber();
        };

        vm.addEmail = function () {
            vm.addedEmails++;
            var emName = vm.addedEmails.toString();
            var email = { value: "", id: vm.addedEmails, name: emName, realId: null };
            vm.emails.push(email);
        };

        vm.removeEmail = function () {
            vm.addedEmails--;
            vm.emails.pop();
        };

        vm.deleteEmail = function (email) {
            if(vm.emails.length > 0){
                $http.delete("/api/emails/" + email.realId).then(function (response) {
                    vm.emails.splice(email.id, 1);
                    vm.addedEmails--;
                    
                }, function () {
                    vm.removeEmail();
                });
            }
        };

        vm.deleteNumber = function (number) {
            if (vm.phoneNumbers.length > 0) {
                $http.delete("/api/numbers/" + number.realId).then(function (response) {
                    if (vm.phoneNumbers.length > 1) {
                        vm.phoneNumbers.splice(number.id, 1);
                        vm.addedNumbers--;
                    }
                    else {
                        vm.phoneNumbers.splice(number.id, 1);
                        var numName = vm.addedNumbers.toString();
                        var num = { value: "", id: vm.addedNumbers, name: numName, realId: null };
                        vm.phoneNumbers.push(num);
                    }

                }, function () {
                    vm.removeNumber();
                });
            }
        };

        vm.setInitialTagValues = function () {
            vm.initialTags.forEach(function (curr) {
                vm.currentTags.push(vm.tags[curr.tagValue].contactTagId);
            });
        };

        vm.clearTags = function () {
            vm.currentTags = [];
            vm.setTag();
        };

        $http.get("/api/tags").then(function (response) {
            vm.tags = response.data;
            vm.setInitialTagValues();
        }, function () {
        });

        $http.get("/api/contacts/" + $routeParams.contactId).then(function (response) {
            vm.editedContact = response.data;
            vm.initialTags = vm.editedContact.tagValues;
            vm.setInitialTagValues();
            vm.editedContact.phoneNumbers.forEach(function (curr, index) {
                var number = { value: curr.number, id: index, name: index.toString, realId: curr.contactPhoneNumberId };
                vm.phoneNumbers.push(number);
            });
            vm.editedContact.emails.forEach(function (curr, index) {
                var email = { value: curr.email, id: index, name: index.toString, realId: curr.contactEmailId };
                vm.emails.push(email);
            });
            vm.addedEmails = vm.editedContact.emails.length - 1;
            vm.addedNumbers = vm.editedContact.phoneNumbers.length - 1;
            vm.initialNumbers = vm.editedContact.phoneNumbers.length - 1;
            vm.initialEmails = vm.editedContact.emails.length - 1;
            vm.setTag();
        }, function () {
            alert("error");
        });

        vm.setTag = function () {
            vm.finishedTags = [];
            vm.currentTags.forEach(function (curr) {
                vm.tags.forEach(function (tg, index) {
                    if (curr == tg.contactTagId) {
                        var tag = { tagValue: index };
                        vm.finishedTags.push(tag);
                    }
                });
            });
        };
        vm.deleteTagValue = function (id) {
            $http.delete("/api/contacts/delTags/" + id).then(function (response) {
            }, function () {
                alert("error");
            });
        };

        vm.editContact = function () {
            vm.initialTags.forEach(function (curr) {
                vm.deleteTagValue(curr.tagId);
            });

            vm.editedContactFinished.contactId = vm.editedContact.contactId;
            vm.editedContactFinished.name = vm.editedContact.name;
            vm.editedContactFinished.adress = vm.editedContact.adress;
            vm.editedContactFinished.birthday = vm.editedContact.birthday;
            vm.editedContactFinished.tagValues = vm.finishedTags;

            vm.phoneNumbers.forEach(function (curr) {
                var num;
                if (curr.realId)
                    num = { number: curr.value, contactPhoneNumberId: curr.realId };
                else
                    num = { number: curr.value};   
                vm.editedContactFinished.phoneNumbers[curr.id] = num;
            });
            vm.emails.forEach(function (curr) {
                var em;
                if (curr.realId)
                    em = { email: curr.value, contactEmailId: curr.realId };
                else
                    em = { email: curr.value};
                vm.editedContactFinished.emails[curr.id] = em;
            });
            $http.put("/api/contacts/" + $routeParams.contactId, JSON.stringify(vm.editedContactFinished)).then(function () {
                $location.path("/");

            }, function () {
                alert("Failed!!!!!");
            });
        };

    }
})();